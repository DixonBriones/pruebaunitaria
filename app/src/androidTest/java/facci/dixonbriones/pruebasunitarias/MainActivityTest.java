package facci.dixonbriones.pruebasunitarias;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.filters.LargeTest;
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

@LargeTest
@RunWith(AndroidJUnit4ClassRunner.class)
public class MainActivityTest
{
    public static final String STRING_CORREO = "dixonbriones@hotmail.com";
    public static final String STRING_PASSWORD = "1234";
    @Rule
    public ActivityScenarioRule<MainActivity> activityActivityScenarioRule = new ActivityScenarioRule<MainActivity>(MainActivity.class);

    @Test
    public void myFirstTest() {
        onView(withId(R.id.correoEditText)).perform(typeText(STRING_CORREO), closeSoftKeyboard());
        onView(withId(R.id.enviarButton)).perform(click());
    }

    @Test
    public void mySecondTest() {
        onView(withId(R.id.enviarButton)).perform(click());
    }

    @Test
    public void myThirdTest() {
        onView(withId(R.id.correoEditText)).perform(typeText(STRING_CORREO), closeSoftKeyboard());
        onView(withId(R.id.passwordEditText)).perform(typeText(STRING_PASSWORD), closeSoftKeyboard());
        onView(withId(R.id.enviarButton)).perform(click());
    }

}
