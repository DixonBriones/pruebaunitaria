package facci.dixonbriones.pruebasunitarias;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import facci.dixonbriones.pruebasunitarias.models.Usuario;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
     private Button enviarButton;
     private EditText correoEditText, passwordEditText;
     private Usuario usuario;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setViews();
        usuario = new Usuario("dixonbriones@hotmail.com","1234");


    }

    private void setViews(){
        enviarButton= findViewById(R.id.enviarButton);
        enviarButton.setOnClickListener(this);
        correoEditText=findViewById(R.id.correoEditText);
        passwordEditText=findViewById(R.id.passwordEditText);
    }
    public boolean findUser() {
        return Objects.equals(usuario.getCorreo(),correoEditText.getText().toString());
    }


    private boolean hasCorreo() {
        return !correoEditText.getText().toString().isEmpty();
    }
    private boolean hasPassword() {
        return !passwordEditText.getText().toString().isEmpty();
    }
    private boolean ValidateCorreo(){
        Pattern pattern = Pattern
                .compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        Matcher mather = pattern.matcher(correoEditText.getText().toString());
        return mather.find();
    }

    @Override
    public void onClick(View v) {
        if (v == enviarButton) {
            if (!hasCorreo() || !hasPassword()) {
                Toast.makeText(this, "Debe ingresar los datos", Toast.LENGTH_SHORT).show();
            } else {
                if (!ValidateCorreo()){
                    Toast.makeText(this, "El correo debe ser valido", Toast.LENGTH_SHORT).show();
                }
                else {
                    if (!findUser()){
                        Toast.makeText(this, "El usuario no existe", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Toast.makeText(this, "Inicio Correcto", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }

    }
}